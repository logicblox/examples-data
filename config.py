#! /usr/bin/env python

from lbconfig.api import *

lbconfig_package('example-app', version='0.1', default_targets=['check-lb-workspaces'])

depends_on(
    logicblox_dep
)

lb_library (
    name = 'project',
    srcdir='.'
)

check_lb_workspace(
    name='lb-example',
    libraries = ['project']
)