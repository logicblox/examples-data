/*****************************************************************************
 * Creating some data for the product hierarchy.
 * 
 * Illustrates:
 * - delta rules
 * - entity creation
 * - fully qualified name referencing predicates declared in modules
 ******************************************************************************/

//creating the brand entities
  +hierarchy:product:brand(b),
  +hierarchy:product:brand_by_name[n] = b <-
    n="Fred's Emporium".

  +hierarchy:product:brand(b),
  +hierarchy:product:brand_by_name[n] = b <-
    n="Queen of Pops".

  +hierarchy:product:brand(b),
  +hierarchy:product:brand_by_name[n] = b <-
    n="Sorbelicious".

//creating the sku entities and mappings to brand
  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Goat Cheese Cashew Caramel",
     hierarchy:product:brand_by_name["Fred's Emporium"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Mexican Chocolate",
     hierarchy:product:brand_by_name["Fred's Emporium"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Sichuan Pepper",
     hierarchy:product:brand_by_name["Fred's Emporium"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Honey Jalapeno Pickle",
     hierarchy:product:brand_by_name["Fred's Emporium"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Coconut Latte",
     hierarchy:product:brand_by_name["Fred's Emporium"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Apple Cobler",
     hierarchy:product:brand_by_name["Fred's Emporium"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Salted Walnut",
     hierarchy:product:brand_by_name["Fred's Emporium"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Smurf Gelato",
     hierarchy:product:brand_by_name["Fred's Emporium"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Wildberry Lavender",
     hierarchy:product:brand_by_name["Fred's Emporium"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="White Truffle Gelato",
     hierarchy:product:brand_by_name["Fred's Emporium"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Banana Curry",
     hierarchy:product:brand_by_name["Fred's Emporium"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Salty Licorice",
     hierarchy:product:brand_by_name["Fred's Emporium"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Tequila Lime",
     hierarchy:product:brand_by_name["Sorbelicious"]=brand.


  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Whiskey Smash",
     hierarchy:product:brand_by_name["Sorbelicious"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Five-Spiced Plum",
     hierarchy:product:brand_by_name["Sorbelicious"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Bloody Mary",
     hierarchy:product:brand_by_name["Sorbelicious"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Hibiscus Tea",
     hierarchy:product:brand_by_name["Sorbelicious"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Pickled Blueberry",
     hierarchy:product:brand_by_name["Sorbelicious"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Quince and Vanilla",
     hierarchy:product:brand_by_name["Sorbelicious"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Zucchini-Lemon",
     hierarchy:product:brand_by_name["Sorbelicious"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Creamy Avocado",
     hierarchy:product:brand_by_name["Queen of Pops"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Caramel Apple",
     hierarchy:product:brand_by_name["Queen of Pops"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Honeydew Lime Zest",
     hierarchy:product:brand_by_name["Queen of Pops"]=brand.
     
  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Coconut Banana Orange",
     hierarchy:product:brand_by_name["Queen of Pops"]=brand.     
     
  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Tangerine Basil",
     hierarchy:product:brand_by_name["Queen of Pops"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Herbs de Provence Lemonade",
     hierarchy:product:brand_by_name["Queen of Pops"]=brand.

  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Kiwi Banana Honey",
     hierarchy:product:brand_by_name["Queen of Pops"]=brand.
     
  +hierarchy:product:sku(s),
  +hierarchy:product:sku_by_name[name] = s,
  +hierarchy:product:sku_in_brand[s] = brand <-
     name="Apple Cider",
     hierarchy:product:brand_by_name["Queen of Pops"]=brand.     